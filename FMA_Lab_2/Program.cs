﻿using System;

namespace FMA_Lab_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("");
            Console.ReadKey();
        }
    }

    class Queue {

        int n;
        int[] Buffer;
        int head, tail;
        bool fl;
        int count;

        public Queue(int n) {
            this.n = n;
            Buffer = new int[n];
            head = tail = 0;
            fl = false;
            count = 0;
        }

        public bool Empty {
            get {
                if (count == 0) return true;
                else return false;
            }
        }

        public void push(int t) {
            int current = head + 1;
            if (current == n)
                current = 0;
            Buffer[current] = t;
            head = current;
            count++;
            if (count == n)
                fl = true;
        }

        public int pop() {
            int current = tail + 1;
            if (current == n)
                current = 0;
            tail = current;

            count--;
            if (count < n)
                fl = false;

            return Buffer[current];
        }

    }

}
